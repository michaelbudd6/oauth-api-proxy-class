<?php 

/** example group for api/proxy **/

Route::group( array( 'prefix' => 'api/proxy', 'middleware' => [ 'web' ] ), function() {

    Route::resource( 'internal/application/load', 'API\Proxy' );
    Route::resource( 'api/v1/books/visit', 'API\Proxy' );

});