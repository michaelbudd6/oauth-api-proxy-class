<?php 

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;

class Proxy extends \App\Http\Controllers\Controller {

	public function index( Request $request ) {

		$this->run( $request );
	}

	public function create( Request $request ) {

		$this->run( $request );		
	}

	public function store( Request $request ) {

		$this->run( $request );		
	}

	public function show( Request $request ) {

		$this->run( $request );		
	}

	public function edit( Request $request ) {

		$this->run( $request );		
	}

	public function update( Request $request ) {

		$this->run( $request );		
	}

	public function destroy( Request $request ) {

		$this->run( $request );		
	}

	public function run( Request $request ) {

		//$request->request->add( [ 'access_token' => session( 'access_token' ) ] );

		$formParams = [];
		$query 		= [ 'access_token' => session( 'access_token' ) ];

		switch( $request->method() ) {

			case( "POST" ) :

				$formParams = array_merge( [ 'access_token' => session( 'access_token' ) ], $request->input() );

				break;

			default :

				$query 	= array_merge( $query, $request->input() );

				break;
		}

		$clientArgs = [

			'base_uri'		=>	env( 'API_SOURCE' ) . '/'
		];


		$clientArgs['query'] = $query;

		if( !empty( $formParams ) ) {

			$clientArgs['form_params'] = $formParams;
		}


		$client = new \GuzzleHttp\Client( $clientArgs );

		$response = $client->request( $request->method(), 
			str_replace( 'api/proxy', '', $request->path() ) );

		echo $response->getBody();
	}
}