# Usage

This is a very simple thin layer proxy for api calls, for Laravel 5. Currently works with Brent Shaffer's OAuth 2 server, but could be used
to work with others.

Requires OAuth token to be set in session with key name "access_token". The proxy layer attaches the access token to all requests.

Forwards all requests to your real end points, the variable API_SOURCE must be set in your .env file.

You can then make API calls ( for example ) in JS like so:

```javascript
var request = new XMLHttpRequest(); 
request.open( "POST", "api/proxy/api/v1/book/views" ); 
request.setRequestHeader( "Content-Type", "application/json;charset=UTF-8" ); 
request.onreadystatechange 	=  function() { 
    if( request.readyState === 4 ) { 
        doSomething( JSON.parse( request.responseText ) ); 
    } 
}; 
request.send( JSON.stringify( yourArgs ) );
```


* The "api/proxy" part is stripped when the proxy forwards the request, so only the end point is required. The base uri is added through the .env variable
